<?php
/**
 * String
 * 
 * @name String
 * @author Mardix
 * @since   Feb 11, 2014
 * 
 * To encode and decode string
 * 
 * $encrypted = (new Encryptor)->encode($myTextToEncrypt);
 * 
 * $decrypted = (new Encryptor)->decode($encrypted); 
 */
//------------------------------------------------------------------------------
namespace Voodoo\Component\Crypto;

use Voodoo\Core\Config;

class String
{    
    const VERSION = "1.4";
    
    /**
     * @desc maximum key (or salt) length for the private key
     * @var int
     */
    const KEYMINLEN = 10;
    const KEYMEXLEN = 56; // mcrypt demand

    /**
     * @desc some mcrypt required variable
     */
    private $td;
    
    /**
     * @desc some mcrypt required variables
     * @warning
     * NEVER CHANGE THIS VALUES ON A RUNNING SYSTEM,
     * because the encryption key is based on these information snf if changed, old data can not be restored.
     */
    private $cypher = MCRYPT_BLOWFISH;

    /**
     * @warning do not use ECB, it's not really secure, see links below
     * @see http://de.wikipedia.org/wiki/Cipher_Feedback_Mode
     * @see http://de.wikipedia.org/wiki/Electronic_Code_Book_Mode
     */
    private $mode   = MCRYPT_MODE_CFB;

    /**
     * @desc private key for your application
     */
    private  $cypherKey = null;

    /**
     * @desc salt used to fill the private key to a total length of 56 characters (if necessary)
     * @var string
     * @warning
     * - NEVER CHANGE THIS VALUE ON A RUNNING SYSTEM,
     *   because it's part of the encryption key and if changed, old data can not be restored.
     * - Also make sure to store this key in another place, f.e. dotProject.
     * - Do not use special chars here, US-ANSI (ASCII 128) only (to avoid problems on charset conversions)
     */
    private $salt = '';

    /**
     * Constructor
     * @param string $key - The key to encode and decode
     * @param string $salt - the salt to encode and decode 
     */
    public function __construct($key = null, $salt = null)
    {
        $this->salt = $salt ?: Config::Component()->get("Crypto.salt");
        $this->setKey($key  ?: Config::Component()->get("Crypto.key"));
    }
    
    /**
     * Set the key to use
     * @param type $key
     * 
     * @throws Exception 
     */
    private function setKey($key)
    {
        if (!extension_loaded("mcrypt")) {
            throw new Exception("Encryptor: mcrypt php extension not 
                found but required");
        }
        
        $this->key = trim($key);
        $this->td = mcrypt_module_open($this->cypher, '', $this->mode, '');
        
        if (strlen($key) < self::KEYMINLEN) {
            throw new Core\Exception("Encryptor: the key is too short, 
                                 expected at minimum of ".self::KEYMINLEN.", 
                                 given ".strlen($key)."" 
                    );
        } else if (strlen($key) > self::KEYMEXLEN) {
            throw new Core\Exception("Encryptor: the key is too long, 
                                 expected at minimum of ".self::KEYMINLEN.", 
                                 given ".strlen($key)."" 
                    );
        }
        $this->cypherKey = substr($key.$this->salt, 0, self::KEYMEXLEN); 
        return $this;
    }

    
    /**
     * @desc enrypt your plaintext with this method, the return value is the encrypted string
     * @throws Exception
     * @return string encrypted, base64-encoded data (increases the data amount, but uses ASCII charachters)
     */
    public function encode($value)
    {
        if (is_null($this->cypherKey)) {
            throw new Exception("no key set");
        }
        
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($this->td), MCRYPT_RAND);
        mcrypt_generic_init($this->td, $this->cypherKey, $iv);
        $crypttext = mcrypt_generic($this->td, $value);
        mcrypt_generic_deinit($this->td);
        return base64_encode($iv . $crypttext);
    }
    
    
    
    /**
     * @desc decrypt your encrypted data with this method, the return value is the plaintext string
     * @throws Encrypt_Mcrypt_Exception

     * @return string plaintext
     */
    public function decode($value)
    {
        $crypttext = $value;
        if (is_null($this->cypherKey)) {
            throw new Exception("no key set");
        }
        $crypttext = base64_decode($crypttext);
        $ivsize = mcrypt_get_iv_size($this->cypher, $this->mode);
        $iv = substr($crypttext, 0, $ivsize);
        $crypttext = substr($crypttext, $ivsize);
        mcrypt_generic_init($this->td, $this->cypherKey, $iv);
        $plaintext = mdecrypt_generic($this->td, $crypttext);
        mcrypt_generic_deinit($this->td);
        return $plaintext;
    }

    /**
     * @desc close mcrypt module, do not call manually
     */
    public function __destruct()
    {
        mcrypt_module_close($this->td);
    }
}
