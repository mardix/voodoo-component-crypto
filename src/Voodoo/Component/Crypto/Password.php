<?php

/**
 * Password
 * 
 * @name Password
 * @author Mardix
 * @since   Feb 11, 2014
 * 
 * (Voodoo\Component\Crypto\Password)->hash($string)
 */

namespace Voodoo\Component\Crypto;

use Voodoo;

class Password
{
    private $algo = PASSWORD_BCRYPT;
    private $options = [];
    
    public function __construct($algo = null, Array $options = [])
    {
        if ($algo) {
            $this->algo = $algo;
        }
        if (!count($this->options)) {
            $option = Voodoo\Core\Config::Component()->get("Crypto");
            $this->options = [
                "salt" => $option["salt"],
                "code" => $option["code"]
            ];                
        }
    }
    
    /**
     * Hash a password
     * 
     * @param type $string
     * @return String
     */
    public  function hash($string)
    {
        return password_hash($string, $this->algo, $this->options);
    }
    
    /**
     * If password need to be rehashed
     * 
     * @param type $hash
     * @return String
     */
    public static function need_rehash($hash)
    {     
        return password_needs_rehash($hash, $this->algo, $this->options);
    }
    
    /**
     * Get the info
     * @param type $hash
     * @return Array
     */
    public static function get_info($hash)
    {
        return password_get_info($hash);
    }

    /**
     * 
     * @param type $password
     * @param type $hash
     * @return type
     */
    public static function verify($password, $hash)
    {
        return password_verify($password, $hash);
    }
}
