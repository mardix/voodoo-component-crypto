
#Voodoo\Component\Crypto

##Setup

Add the code below in the file: /App/_conf/Component.conf.php

    [Crypto]
        salt = '#89.pp767bgx__8edrfpn4^N%KBRVJH$#%I ^N:LM(){NV CQX-][":.7n36F;-' ; Salt for hashing. salt used to fill the private key to a total length of 56 characters 
        cost = 10 ; Password only. denotes the algorithmic cost that should be used
        key = "" ; String only.

Example:

    (Voodoo\Component\Crypto\String)->encode($string)

    (Voodoo\Component\Crypto\String)->decode($string)

    (Voodoo\Component\Crypto\Password)->hash($string)

    (Voodoo\Component\Crypto\Password)->verify($string, $hash)