<?php

namespace Voodoo\Component\Crypto;

/**
 * Test class for Exception.
 * Generated by PHPUnit on 2014-02-12 at 03:47:18.
 */
class ExceptionTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Exception
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        $this->object = new Exception;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

}

?>
